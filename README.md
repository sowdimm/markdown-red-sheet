# マークダウン赤シートスクリプト

gitlabのプライベートリポジトリにcsvファイルがある場合、そのcsvファイルのあるパス、レポジトリの project id 、アカウントの private token を用意して、
`https://sowdimm.gitlab.io/markdown-red-sheet/csvViewer.html?branch=master&path=[path to csv in your repository]&projectid=[the project id of the repository]&privateToken=[your private token]`
にアクセスすることでcsvが読み込まれ、解答が隠せるようになる。

If you have a .csv in your private repository of GitLab, with the file path, the project id of the repository, and a private token of your GitLab account, access `see above` and you can get the .csv loaded to hide the answer column.

csvファイルがローカルにある場合、`https://sowdimm.gitlab.io/markdown-red-sheet/csvOpener.html`にアクセスすることでローカル内で解答を隠す事ができる。

If you have a .csv in local, access `see above` and you can get the .csv loaded to hide the answer column.

## Todo

- 文字コードの検出、shift-jisの場合エラー投げ
