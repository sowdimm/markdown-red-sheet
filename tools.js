
function getUrlVars () {
	vars = {};
	const param = location.search.substring(1).split("&");
	for( const p of param ){
		keySearch = p.search(/=/);
		key = "";
		if (keySearch != -1) key = p.slice(0,keySearch);
		val = p.slice(p.indexOf("=", 0) + 1);
		if(key != "") vars[key] = decodeURI(val);
	}
	return vars;
}
function hidestrong() {
	var strongs = document.getElementsByTagName("strong");
	for (let i = 0; i < strongs.length; i++) {
		if ( strongs[i].style["background-color"] === "black" )
			strongs[i].style["background-color"] = "white";
		else {
			strongs[i].style["background-color"] = "black";
			strongs[i].style["color"] = "black";
		}
	}
}
function restrictImgs () {
	const imgs = document.getElementsByTagName("img");
	for (let i = 0; i < imgs.length; i++) {
		imgs[i].width = Math.min(imgs[i].naturalWidth, window.parent.screen.width * 0.8);
	}
}

function accessFromSmartDevice () {
	return navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/);
}

function hideButtonSizeForSmartDevice () {
	return Math.floor(Math.min(window.parent.screen.width, window.parent.screen.height) * 0.3 / 2);
}

function csv2md(src, opt) {// code from [csvをmarkdownのテーブル &amp; HTMLに変換するスクリプト | 404 Motivation Not Found](https://tech-blog.s-yoshiki.com/2018/11/742/)
	var del = ","
	var eol = "\n"

	var dst = ""

	src = src.split(eol).map(e => e.split(del))

	if (src.length < 2) {
		return ""
	}

	for (var i = 0; i < src.length; i++) {
		if (i === 1) {
			dst += "|"
			for (var j = 0; j < src[0].length; j++) {
				dst += "---|"
			}
			dst += eol
		}
		var tmp = ""
		tmp = src[i].join("|")
		tmp = "|" + tmp + "|"
		dst += tmp + eol

	}
	return dst
}

function arrayToHTMLTable (dataArr) {
	tableFrameWhole = x => "<table border=1>\n" + x + "</table>\n";
	tableFrameRow = x => "<tr>\n" + x + "</tr>\n";
	tableFrameCell = x => "\t<td>\n\t\t" + x + "\t</td>\n";
	reducer = (accumulator, currentValue) => accumulator + currentValue;
	tableRow = xa => tableFrameRow(xa.map(tableFrameCell).reduce(reducer));
	return tableFrameWhole(dataArr.map(tableRow));
}

function peelText (peeled) {
	while (true) {
		l = peeled.length;
		if (l === 0) break;
		if (peeled[0] === " ") {
			peeled = peeled.slice(1)
			continue;
		} else if (peeled[l-1] === " ") {
			peeled = peeled.slice(0, -1)
			continue;
		} else {
			break;
		}
	}
	return peeled;
}

function empText (textData) {
	empedText = peelText(textData);
	empedText = "<strong>" + empedText + "</strong>";
	return empedText;
}

function empAns (col, emp) {return function (record) {
	tmprecord = record;
	tmprecord[col] = emp(record[col]);
	return tmprecord;
};}


function empAnsCol (csvText, ansText) {
	const parsed = $.csv.toArrays(csvText);
	const ansCol = parsed[0].indexOf(ansText);
	const parsedEmped = parsed.map(empAns(ansCol, empText));
	return $.csv.fromArrays(parsedEmped);
}

function empAnsColToHTML (csvText, ansText) {
	const parsed = $.csv.toArrays(csvText);
	const ansCol = parsed[0].indexOf(ansText);
	const parsedEmped = parsed.map(empAns(ansCol, empText));
	return arrayToHTMLTable(parsedEmped);
}
